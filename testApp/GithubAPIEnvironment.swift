//
//  GithubAPIEnvironment.swift
//  TestApp
//
//  Created by vmilen on 6/2/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class GithubAPIEnvironment: NSObject, APIEnvironment {
    var gists : String {
        return "https://api.github.com/gists/public"
    }
}
