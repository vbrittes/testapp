//
//  APIEnvironment.swift
//  TestApp
//
//  Created by vmilen on 6/2/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

protocol APIEnvironment {
    var gists : String { get }
}
