//
//  GistDetailViewModel.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class GistDetailViewModel: NSObject {
    private let model = GistDetailModel()
    
    var filesCount : Int {
        return model.gist?.files?.count ?? 0
    }
    
    func setNavigationObject(object: GistNavigationObject) {
        let gist = object as GistObject
        
        model.gist = gist
    }
    
    func populateGistDescriber(describer: GistDescriber) {
        var describer = describer //create writable reference
        
        guard let gist = model.gist else {
            return
        }
        
        describer.gistOwnerName = gist.ownerName ?? "Anonymous"
        describer.gistType = gist.type ?? "Unavailable"
        describer.gistLanguage = gist.language ?? "Unavailable"
        
        if let ownerPhoto = gist.ownerPhoto, url = NSURL(string: ownerPhoto) {
            describer.loadOwnerPhotoImageWithUrl(url)
        } else {
            describer.loadOwnerPhotoImageWithUrl(nil)
        }
    }
    
    func populateFileDescriber(describer: FileDescriber, index: Int) {
        var describer = describer //create writable reference
        
        guard let gist = model.gist else {
            return
        }
        
        guard let files = gist.files where files.count > index else {
            return
        }
        
        let file = files[index]
        
        describer.name = file.fileName
        describer.type = file.type
        describer.language = file.language
        describer.content = file.content ?? nil
    }
}
