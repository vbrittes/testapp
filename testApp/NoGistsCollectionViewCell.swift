//
//  NoGistsCollectionViewCell.swift
//  TestApp
//
//  Created by vmilen on 6/5/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class NoGistsCollectionViewCell: UICollectionViewCell {
    
    var message : String? {
        get {
            return messageLabel?.text
        }
        set {
            messageLabel?.text = newValue
        }
    }
    
    @IBOutlet weak var messageLabel: UILabel?
    
    override func awakeFromNib() {
        messageLabel?.text = String()
    }
}
