//
//  GistsViewController.swift
//  testApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class GistsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, GistsDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private let viewModel = GistsViewModel()
    private var emptyContentCell : NoGistsCollectionViewCell? {
        if let noGistCell = collectionView.visibleCells().first as? NoGistsCollectionViewCell {
            return noGistCell
        }
        return nil
    }
    
    //MARK: Lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        viewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        viewModel.delegate = self
    }
    
    //MARK: Default
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.contentInset = UIEdgeInsets(top: collectionView.contentInset.top, left: collectionView.contentInset.left, bottom: collectionView.contentInset.bottom + 44, right: collectionView.contentInset.right)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.fetchGists(0)
    }
    
    //MARK: Custom error handling
    override func displayAlert(title: String, message: String, critical: Bool) {
        if critical {
            super.displayAlert(title, message: message, critical: critical)
        } else {
            emptyContentCell?.message = message
        }
    }
    
    //MARK: GistsDelegate
    func gistsUpdated() {
        collectionView.reloadData()
    }
    
    //MARK: UICollectionViewDelegate & UICollectionDataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.gistsCount == 0 ? 1 : viewModel.numberOfItemsInSection(section)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCellWithReuseIdentifier(viewModel.gistsCount == 0 ? String(NoGistsCollectionViewCell) : String(GistCollectionViewCell), forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = cell as? GistCollectionViewCell {
            viewModel.populateGistDescriber(cell, item: indexPath.row, section: indexPath.section)
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return viewModel.gistsCount == 0 ? 1 : Int(ceil(Double(viewModel.gistsCount)/Double(viewModel.maxItemsPerSection)))
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if viewModel.gistsCount == 0 {
            let statusBar = CGFloat(20.0)
            if let nav = navigationController?.navigationBar.frame.height {
                return CGSize(width: collectionView.frame.width, height: collectionView.frame.height - nav - statusBar)
            }
            
            return collectionView.frame.size
        }
        
        let side = collectionView.frame.width / CGFloat(viewModel.maxItemsPerSection)
        return CGSize(width: side, height: side)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if viewModel.gistsCount == 0 {
            viewModel.fetchGists(0)
            emptyContentCell?.message = String()
        }
    }
    
    //MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let forwardVC = segue.destinationViewController as? GistNavigationAware {
            if let gistDescriber = sender as? GistDescriber, index = gistDescriber.index {
                forwardVC.forwardNavigationObject(viewModel.dataForNavigation(index))
            }
        }
    }
    
}
