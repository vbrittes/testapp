//
//  DataFlowTypes.swift
//  TestApp
//
//  Created by vmilen on 6/7/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

typealias RequestSuccessResult = (AnyObject?) -> Void
typealias RequestErrorResult = (NSError?) -> Void
typealias RequestProgress = (NSProgress) -> Void
