//
//  AFNetworkingHTTPPerformer.swift
//  TestApp
//
//  Created by vmilen on 6/4/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import AFNetworking

class AFNetworkingHTTPPerformer: NSObject, HTTPPerformer {
    
    func request(method: HTTPMethod, url: String, parameters: [String : AnyObject]?, success: RequestSuccessResult?, error: RequestErrorResult?, header: [String: String]? = nil, progress: RequestProgress? = nil) {
        let sessionManager = AFHTTPSessionManager()
        
        sessionManager.requestSerializer = AFJSONRequestSerializer()
        sessionManager.responseSerializer.acceptableContentTypes = nil
        
        if let headers = header {
            for (field, value) in headers {
                sessionManager.requestSerializer.setValue(value, forHTTPHeaderField: field)
            }
        }
        
        switch method {
        case .Get:
            get(sessionManager, url: url, parameters: parameters, success: success, error: error)
        case .Post:
            post(sessionManager, url: url, parameters: parameters, success: success, error: error)
        case .Put:
            put(sessionManager, url: url, parameters: parameters, success: success, error: error)
        case .Delete:
            delete(sessionManager, url: url, parameters: parameters, success: success, error: error)
        }
    }
    
    private func get(manager: AFHTTPSessionManager, url: String, parameters: AnyObject?, success: RequestSuccessResult?, error: RequestErrorResult?, progress: RequestProgress? = nil) {
        
        manager.GET(url, parameters: parameters, progress: progress, success: { (task, result) -> Void in
            success?(result)
        }) { (task, result) -> Void in
            error?(result)
        }
    }
    
    private func post(manager: AFHTTPSessionManager, url: String, parameters: AnyObject?, success: RequestSuccessResult?, error: RequestErrorResult?, progress: RequestProgress? = nil) {
        manager.POST(url, parameters: parameters, progress: progress, success: { (task, result) -> Void in
            success?(result)
        }) { (task, result) -> Void in
            error?(result)
        }
    }
    
    private func put(manager: AFHTTPSessionManager, url: String, parameters: AnyObject?, success: RequestSuccessResult?, error: RequestErrorResult?, progress: RequestProgress? = nil) {
        manager.PUT(url, parameters: parameters, success: { (task, result) -> Void in
            success?(result)
        }) { (task, result) -> Void in
            error?(result)
        }
    }
    
    private func delete(manager: AFHTTPSessionManager, url: String, parameters: AnyObject?, success: RequestSuccessResult?, error: RequestErrorResult?, progress: RequestProgress? = nil) {
        manager.DELETE(url, parameters: parameters, success: { (task, result) -> Void in
            success?(result)
        }) { (task, result) -> Void in
            error?(result)
        }
    }
    
}
