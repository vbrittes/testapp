//
//  GistRemoteService.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit
import JSONHelper

class GistRemoteService: RemoteService, GistService {
    func fetchGists(page: Int, limit: Int, completion: GistCompletion?) {
        httpPerformer.request(.Get, url: apiEnvironment.gists, parameters: ["page" : page, "per_page" : limit], success: { (result) in
            if let result = result as? [JSONDictionary] {
                let gists = result.map{ GistObject(data: $0) }
                completion?(result: gists, append: true, error: nil)
            }
            }, error: { (error) in
                completion?(result: nil, append: true, error: error)
            }, header: nil, progress: nil)
    }
}
