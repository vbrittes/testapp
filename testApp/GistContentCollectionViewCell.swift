//
//  GistContentCollectionViewCell.swift
//  TestApp
//
//  Created by vmilen on 6/8/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class GistContentCollectionViewCell: UICollectionViewCell, FileDescriber {
    
    var name : String? {
        get {
            return nameLabel.text
        }
        set {
            nameLabel.text = newValue
        }
    }
    
    var language : String? {
        get {
            return languageLabel.text
        }
        set {
            languageLabel.text = newValue
        }
    }
    
    var type : String? {
        get {
            return typeLabel.text
        }
        set {
            typeLabel.text = newValue
        }
    }
    
    var content : String? {
        get {
            return contentTextView.text
        }
        set {
            contentTextView.text = newValue
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    override func prepareForReuse() {
        name = nil
        language = nil
        type = nil
        content = nil
    }
    
    override func awakeFromNib() {
        contentTextView.contentInset = UIEdgeInsets(top: headerHeightConstraint.constant, left: 0, bottom: 16, right: 0)
    }
    
}
