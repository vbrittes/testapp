//
//  OwnerObject.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit
import JSONHelper
import RealmSwift
import Realm

class OwnerObject: Object, Deserializable {
    dynamic var login: String?
    dynamic var avatarUrl: String?
    
    required init(data: JSONDictionary) {
        super.init()
        
        login <-- data["login"]
        avatarUrl <-- data["avatar_url"]
    }
    
    required init() {
        super.init()
    }
    
    required init(value: AnyObject, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
}
