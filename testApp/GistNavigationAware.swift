//
//  GistNavigationAware.swift
//  TestApp
//
//  Created by vmilen on 6/4/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

typealias GistNavigationObject = GistObject

protocol GistNavigationAware {
    func forwardNavigationObject(object: GistNavigationObject)
}
