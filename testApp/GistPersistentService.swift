//
//  GistPersistentService.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class GistPersistentService: NSObject, GistService {
    
    private let remoteService : GistService?
    private let localService : GistLocalService
    
    init(remoteService: GistService? = GistRemoteService(), localService: GistLocalService = GistLocalService()) {
        self.remoteService = remoteService
        self.localService = localService
    }
    
    func fetchGists(page: Int, limit: Int, completion: GistCompletion?) {
        if let remoteService = remoteService {
            remoteService.fetchGists(page, limit: limit, completion: { [weak self] (result, append, error) in
                
                if let error = error {
                    self?.fallbackToLocal(completion, remoteError: error)
                    return
                }
                
                guard let result = result else {
                    self?.fallbackToLocal(completion, remoteError: nil)
                    return
                }
                
                if page == 0 {
                    completion?(result: [], append: false, error: nil)
                    self?.localService.removeAllGists(nil)
                }
                
                for r in result {
                    self?.localService.saveGist(r, completion: nil)
                }
                
                completion?(result: result, append: true, error: error)
            })
        } else if page == 0 {
            fallbackToLocal(completion, remoteError: nil)
        }
    }
    
    private func fallbackToLocal(completion: GistCompletion?, remoteError: NSError?) {
        //this resets all results
        completion?(result: [], append: false, error: remoteError)
        
        localService.fetchGists(0, limit: 10, completion: { (result, append, error) in
            completion?(result: result, append: false, error: remoteError)
        })
    }
}
