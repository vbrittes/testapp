//
//  GistDetailViewController.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class GistDetailViewController: UIViewController, GistNavigationAware, GistDescriber, UICollectionViewDelegate, UICollectionViewDataSource {
    
    private let viewModel = GistDetailViewModel()
    
    var index: Int?
    
    var gistOwnerName: String? {
        get {
            return ownerLabel?.text
        }
        set {
            ownerLabel?.text = newValue
        }
    }
    
    var gistType: String? {
        get {
            return typeLabel?.text
        }
        set {
            typeLabel?.text = newValue
        }
    }
    
    var gistLanguage: String? {
        get {
            return languageLabel?.text
        }
        set {
            languageLabel?.text = newValue
        }
    }
    
    @IBOutlet weak var ownerLabel: UILabel?
    @IBOutlet weak var typeLabel: UILabel?
    @IBOutlet weak var languageLabel: UILabel?
    
    @IBOutlet weak var backgroundImageView: UIImageView?
    @IBOutlet weak var focusImageView: UIImageView?
    
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBInspectable var defaultPlaceholderImage : UIImage?
    
    func loadOwnerPhotoImageWithUrl(url: NSURL?) {
        if let url = url {
            backgroundImageView?.setImageWithURL(url, placeholderImage: defaultPlaceholderImage)
            focusImageView?.setImageWithURL(url, placeholderImage: defaultPlaceholderImage)
        } else {
            backgroundImageView?.image = defaultPlaceholderImage
            focusImageView?.image = defaultPlaceholderImage
        }
        
    }
    
    func forwardNavigationObject(object: GistNavigationObject) {
        viewModel.setNavigationObject(object)
        viewModel.populateGistDescriber(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.populateGistDescriber(self)
    }
    
    //MARK: UICollectionViewDelegate & UICollectionDataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCellWithReuseIdentifier(String(GistContentCollectionViewCell), forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = cell as? GistContentCollectionViewCell {
            viewModel.populateFileDescriber(cell, index: indexPath.section)
        }
        
        pageControl.numberOfPages = viewModel.filesCount
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return viewModel.filesCount
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.width)
    }
    
}
