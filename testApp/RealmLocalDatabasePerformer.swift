//
//  RealmLocalDatabasePerformer.swift
//  TestApp
//
//  Created by vmilen on 6/7/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class RealmLocalDatabasePerformer: NSObject {
    func writeObject(object: Object, success: RequestSuccessResult?, error: RequestErrorResult?) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(object)
        }
        success?(object)
    }
    
    func fetchAll<T : Object>(type: T.Type, success: RequestSuccessResult?, error: RequestErrorResult?) {
        let realm = try! Realm()
        
        let gists = realm.objects(T.self)
        
        success?(gists)
    }
    
    func deleteAll<T : Object>(type: T.Type, success: RequestSuccessResult?, error: RequestErrorResult?) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.deleteAll()
        }
        success?(nil)
    }
}
