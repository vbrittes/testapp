//
//  GistsViewModel.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class GistsViewModel: NSObject {
    
    private let model = GistsModel()
    
    private let itemsPerPage = 20
    private let itemsToLoadNextPage = 10
    
    let maxItemsPerSection = 2
    
    weak var delegate : GistsDelegate? {
        get {
            return model.delegate
        }
        set {
            model.delegate = newValue
        }
    }
    
    var gistsCount : Int {
        return model.gists.count
    }
    
    func fetchGists(page: Int) {
        model.fetchGists(page, limit: itemsPerPage)
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        let previousItems = section * maxItemsPerSection
        let remainingItems = model.gists.count - previousItems
        
        return remainingItems >= maxItemsPerSection ? maxItemsPerSection : remainingItems
    }
    
    func populateGistDescriber(describer: GistDescriber, item: Int, section: Int) {
        var describer = describer //create writable reference
        
        assert(!model.gists.isEmpty, "This should not be called if there are no fetched gists")
        
        let index = section * maxItemsPerSection + item
        let gist = model.gists[index]
        
        describer.index = index
        
        describer.gistOwnerName = gist.ownerName ?? "Anonymous"
        describer.gistType = gist.type ?? "Unavailable"
        describer.gistLanguage = gist.language ?? "Unavailable"
        
        if let ownerPhoto = gist.ownerPhoto, url = NSURL(string: ownerPhoto) {
            describer.loadOwnerPhotoImageWithUrl(url)
        } else {
            describer.loadOwnerPhotoImageWithUrl(nil)
        }
        
        if index == model.gists.count - itemsToLoadNextPage {
            model.fetchGists(Int(ceil(Double(index)/Double(itemsPerPage))), limit: itemsPerPage)
        }
    }
    
    func dataForNavigation(index: Int) -> GistNavigationObject {
        assert(!model.gists.isEmpty, "This should not be called if there are no fetched gists")
        
        let gist = model.gists[index]
        
        return gist
    }
}
