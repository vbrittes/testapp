//
//  HTTPPerformer.swift
//  TestApp
//
//  Created by vmilen on 6/2/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

enum HTTPMethod : String {
    case Get = "GET"
    case Post = "POST"
    case Put = "PUT"
    case Delete = "DELETE"
}

protocol HTTPPerformer {
    func request(method: HTTPMethod, url: String, parameters: [String : AnyObject]?, success: RequestSuccessResult?, error: RequestErrorResult?, header: [String: String]?, progress: RequestProgress?)
}
