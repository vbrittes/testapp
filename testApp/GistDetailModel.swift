//
//  GistDetailModel.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class GistDetailModel: NSObject {
    
    var gist: GistObject? {
        didSet {
            downloadFilesContents()
        }
    }
    
    private func downloadFilesContents() {
        guard let files = gist?.files else {
            return
        }
        
        for f in files {
            if let urlString = f.rawUrl, url = NSURL(string: urlString) {
                do {
                    f.content = try String(contentsOfURL: url, encoding: NSUTF8StringEncoding)
                } catch {
                    f.content = nil
                }
            }
        }
    }
    
}
