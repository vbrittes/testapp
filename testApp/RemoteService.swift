//
//  RemoteService.swift
//  TestApp
//
//  Created by vmilen on 6/5/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class RemoteService: NSObject {
    let httpPerformer : HTTPPerformer
    let apiEnvironment : APIEnvironment
    
    init(httpPerformer: HTTPPerformer = AFNetworkingHTTPPerformer(), apiEnvironment: APIEnvironment = GithubAPIEnvironment()) {
        self.httpPerformer = httpPerformer
        self.apiEnvironment = apiEnvironment
    }
}
