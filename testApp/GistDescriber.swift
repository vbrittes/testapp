//
//  GistDescriber.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

protocol GistDescriber {
    var index: Int? { get set }
    
    var gistOwnerName: String? { get set }
    var gistType: String? { get set }
    var gistLanguage: String? { get set }
    
    func loadOwnerPhotoImageWithUrl(url: NSURL?)
}
