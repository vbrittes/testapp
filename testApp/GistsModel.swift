//
//  GistsModel.swift
//  testApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

protocol GistsDelegate : class, ErrorAware {
    func gistsUpdated()
}

class GistsModel: NSObject {
    
    let gistService: GistService
    var gists = [GistObject]()
    
    weak var delegate : GistsDelegate?
    
    init(gistService: GistService = GistPersistentService()) {
        self.gistService = gistService
    }
    
    func fetchGists(page: Int, limit: Int) {
        if (page + 1) <= Int(ceil(Double(gists.count)/Double(limit))) && !gists.isEmpty {
            return
        }
        
        gistService.fetchGists(page, limit: limit) { [weak self] (result, append, error) in
            guard let weakSelf = self else {
                return
            }
            
            if let result = result {
                if append {
                    weakSelf.gists.appendContentsOf(result)
                } else {
                    weakSelf.gists = result
                }
            } else if let error = error {
                weakSelf.delegate?.displayAlert("TestApp", message: error.localizedDescription, critical: false)
            }
            weakSelf.delegate?.gistsUpdated()
        }
    }
    
}
