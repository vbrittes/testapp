//
//  FileDescriber.swift
//  TestApp
//
//  Created by vmilen on 6/8/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

protocol FileDescriber {
    var name : String? { get set }
    var language : String? { get set }
    var type : String? { get set }
    var content : String? { get set }
}
