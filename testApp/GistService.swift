//
//  GistService.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

typealias GistCompletion = (result: [GistObject]?, append: Bool, error: NSError?) -> Void

protocol GistService {
    func fetchGists(page: Int, limit: Int, completion: GistCompletion?)
}
