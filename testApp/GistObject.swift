//
//  GistObject.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit
import JSONHelper
import RealmSwift
import Realm

class GistObject: Object, Deserializable {
    
    //data model attributes
    dynamic var owner : OwnerObject?
    var files : List<FileObject>?
    
    //usefull getters
    var ownerName : String? {
        return owner?.login
    }
    
    var ownerPhoto : String? {
        return owner?.avatarUrl
    }
    
    var type : String? {
        if let files = files where !files.isEmpty {
            let uniqueFileTypes = Array(Set(files.flatMap { $0.type }))
            return uniqueFileTypes.isEmpty ? nil : uniqueFileTypes.joinWithSeparator(", ")
        }
        return nil
    }
    
    var language : String? {
        if let files = files where !files.isEmpty {
            let uniqueFileLanguages = Array(Set(files.flatMap { $0.language }))
            return uniqueFileLanguages.isEmpty ? nil : uniqueFileLanguages.joinWithSeparator(", ")
        }
        return nil
    }
    
    //Lifecycle
    required init(data: JSONDictionary) {
        super.init()
        owner <-- data["owner"]
        
        var filesData : [String: JSONDictionary]?
        filesData <-- data["files"]
        
        if let filesData = filesData {
            files = List<FileObject>()
            for f in filesData.values.map({ FileObject(data: $0) }) {
                files!.append(f)
            }
        }
    }
    
    required init() {
        super.init()
    }
    
    required init(value: AnyObject, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
}
