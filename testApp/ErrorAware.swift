//
//  ErrorAware.swift
//  TestApp
//
//  Created by vmilen on 6/5/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

protocol ErrorAware {
    func displayAlert(title: String, message: String, critical: Bool)
}

extension UIViewController : ErrorAware {
    
    func displayAlert(title : String, message : String, critical : Bool) {
        if critical {
            displayCriticalAlert(title, message: message)
        } else {
            displayNonCriticalAlert(title, message: message)
        }
    }
    
    private func displayCriticalAlert(title : String, message : String) {
        if presentedViewController != nil {
            return
        }
        
        let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addAction(okAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func displayNonCriticalAlert(title : String, message : String) {
        //here we can add some kind of "non-invasive alert" like CSNotificationView
        
        if presentedViewController != nil {
            return
        }
        
        let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addAction(okAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}
