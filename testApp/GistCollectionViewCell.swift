//
//  GistCollectionViewCell.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit
import AFNetworking

class GistCollectionViewCell: UICollectionViewCell, GistDescriber {
    
    var index: Int? {
        didSet {
            let fixed = index != 0 ? CGFloat(index!) : CGFloat(0.01)
            
            let color = UIColor(hue: CGFloat((fixed%180)/360), saturation: 0.5, brightness: 1, alpha: 1)
            backgroundColor = color
        }
    }
    
    var gistOwnerName: String? {
        get {
            return ownerNameLabel.text
        }
        set {
            ownerNameLabel.text = newValue
        }
    }
    
    var gistType: String? {
        get {
            return typeLabel.text
        }
        set {
            typeLabel.text = newValue
        }
    }
    
    var gistLanguage: String? {
        get {
            return languageLabel.text
        }
        set {
            languageLabel.text = newValue
        }
    }
    
    @IBInspectable var placeholderImage : UIImage?
    
    @IBOutlet weak var ownerPhotoImageView: UIImageView!
    
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    
    func loadOwnerPhotoImageWithUrl(url: NSURL?) {
        guard let url = url else {
            ownerPhotoImageView.image = placeholderImage
            return
        }
        ownerPhotoImageView.setImageWithURL(url, placeholderImage: placeholderImage)
    }
    
    override func prepareForReuse() {
        ownerPhotoImageView.cancelImageDownloadTask()
        ownerPhotoImageView.image = placeholderImage
    }
    
}
