//
//  GistLocalService.swift
//  TestApp
//
//  Created by vmilen on 6/6/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

typealias LocalGistCompletion = (result: [GistObject]?, error: NSError?) -> Void

class GistLocalService: NSObject, GistService {
    
    let localDatabasePerformer = RealmLocalDatabasePerformer()
    
    func saveGist(gist: GistObject, completion: LocalGistCompletion?) {
        localDatabasePerformer.writeObject(gist, success: { (result) in
            completion?(result: [gist], error: nil)
            }) { (error) in
                completion?(result: nil, error: error)
        }
    }
    
    func removeAllGists(completion: LocalGistCompletion?) {
        localDatabasePerformer.deleteAll(GistObject.self, success: nil, error: nil)
    }
    
    func fetchGists(page: Int, limit: Int, completion: GistCompletion?) {
        localDatabasePerformer.fetchAll(GistObject.self, success: { (obj) in
            guard let gistsResult = obj as? Results<GistObject> else {
                completion?(result: nil, append: false, error: nil)
                return
            }
            completion?(result: Array(gistsResult), append: false, error: nil)
            }, error: { (error) in
                completion?(result: nil, append: false, error: error)
        })
    }
}
