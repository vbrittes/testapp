//
//  FileObject.swift
//  TestApp
//
//  Created by vmilen on 6/1/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit
import JSONHelper
import RealmSwift
import Realm

class FileObject: Object, Deserializable {
    dynamic var fileName : String?
    dynamic var type : String?
    dynamic var language : String?
    dynamic var rawUrl : String?
    dynamic var content : String?
    
    required init(data: JSONDictionary) {
        super.init()
        
        fileName <-- data["filename"]
        type <-- data["type"]
        language <-- data["language"]
        rawUrl <-- data["raw_url"]
    }
    
    required init() {
        super.init()
    }
    
    required init(value: AnyObject, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
}
