//
//  TestGistObject.swift
//  TestApp
//
//  Created by vmilen on 6/6/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import XCTest
import JSONHelper

class TestGistObject: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDeserialization() {
        let ownerLogin = "ownerTestLogin"
        let ownerAvatarUrl = "ownerAvatarUrl"
        
        let fileType = "fileType"
        let fileLanguage = "fileLanguage"
        let fileName = "fileName"
        
        let ownerMock = ["login" : ownerLogin,
                         "avatar_url" : ownerAvatarUrl]
        
        let filesMock = [fileName :
            [
                "filename" : fileName,
                "language" : fileLanguage,
                "type" : fileType
            ]
        ]
        
        let gistMock = ["owner" : ownerMock,
                        "files" : filesMock]
        
        let gist = GistObject(data: gistMock as! JSONDictionary)
        
        XCTAssertEqual(gist.owner?.login, ownerLogin)
        XCTAssertEqual(gist.owner?.avatarUrl, ownerAvatarUrl)
        
        XCTAssertEqual(gist.files?.first?.fileName, fileName)
        XCTAssertEqual(gist.files?.first?.language, fileLanguage)
        XCTAssertEqual(gist.files?.first?.type, fileType)
    }
    
}
