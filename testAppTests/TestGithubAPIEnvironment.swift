//
//  TestGithubAPIEnvironment.swift
//  TestApp
//
//  Created by vmilen on 6/5/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import XCTest

class TestGithubAPIEnvironment: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testValidGistUtl() {
        let url = NSURL(string: GithubAPIEnvironment().gists)
        XCTAssertNotNil(url, "The url string cannot be converted to a valid URL")
    }
}
