//
//  TestAFNetworkingHTTPPerformer.swift
//  TestApp
//
//  Created by vmilen on 6/5/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import XCTest

class TestAFNetworkingHTTPPerformer: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetRequest() {
        let testPath = "https://itunes.apple.com/search?term=apple"
        let description = "AFNetworkingHTTPPerformer.get"
        let performer = AFNetworkingHTTPPerformer()
        
        let expectation = expectationWithDescription(description)
        
        performer.request(.Get, url: testPath, parameters: nil, success: { (result) in
            XCTAssert(true)
            expectation.fulfill()
            }, error:{ (error) in
            XCTAssert(false, "Request error: \(error?.localizedDescription)")
        })
        
        waitForExpectationsWithTimeout(10, handler: nil)
    }
}
