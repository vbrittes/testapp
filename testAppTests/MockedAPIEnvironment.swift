//
//  MockedAPIEnvironment.swift
//  TestApp
//
//  Created by vmilen on 6/6/16.
//  Copyright © 2016 vbrittes. All rights reserved.
//

import UIKit

class MockedAPIEnvironment: NSObject, APIEnvironment {
    var gists : String {
        return NSBundle.mainBundle().pathForResource("gists-mock", ofType: "json")!
    }
}
